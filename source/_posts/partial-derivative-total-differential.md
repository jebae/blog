---
title: 편미분과 전미분
tags:
  - 수학
  - 미적분
categories:
  - 수학
  - 미적분
date: 2019-02-25 11:39:50
---


<br>
### 편미분

편미분(partial derivative)은 partial이라는 단어에서 의미하듯 한쪽에 편중해서 미분하겠다는 의미입니다. 함수 $F(x, y) = f(x) + g(y) + h(x)k(y)$가 있을 때 $x$에 대해 편미분한 식은 다음과 같습니다.

$$
\begin{align}
\frac{\partial F}{\partial x} & = \frac{\partial f}{\partial x} + \frac{\partial h}{\partial x} k(y) \\\
& = f^\prime(x) + h^\prime(x) g(y)
\end{align}
$$

$x$에 대해 편미분할 때 변수 $y$는 상수취급 합니다.
$y$에 대해 편미분한 식은 아래와 같습니다.

$$
\begin{align}
\frac{\partial F}{\partial y} & = \frac{\partial g}{\partial y} + \frac{\partial k}{\partial y} h(x) \\\
& = g^\prime(y) + k^\prime(y) h(x)
\end{align}
$$

<br><br>
### 전미분

전미분(total differential)은 모두 미분한다는 뜻입니다.
함수 $t = t(x, y)$와 $F(x, y) = f(x) + g(y) + h(x)k(y)$ 가 있을 때 $t$에 대해 전미분한 식은 다음과 같습니다.

$$
\begin{align}
\frac{dF}{dt} & = \frac{df}{dt} + \frac{dg}{dt} + \frac{d(h(x) k(y))}{dt} \\\ \\\
& = \frac{df}{dx} \frac{dx}{dt} + \frac{dg}{dy} \frac{dy}{dt} + \frac{dh}{dx} \frac{dx}{dt} k(y) + \frac{dk}{dy} \frac{dy}{dt} h(x) \\\ \\\
& = \begin{bmatrix}\frac{df}{dx} + \frac{dh}{dx} k(y)\end{bmatrix} \frac{dx}{dt} + \begin{bmatrix}\frac{dg}{dy} + \frac{dk}{dy} h(x)\end{bmatrix} \frac{dy}{dt} \\\ \\\
& = \frac{\partial F}{\partial x} \frac{dx}{dt} + \frac{\partial F}{\partial y} \frac{dy}{dt}
\end{align}
$$
$$
\therefore dF = \frac{\partial F}{\partial x} dx + \frac{\partial F}{\partial y} dy
$$

$\frac{df}{dt} = \frac{df}{dx} \frac{dx}{dt}$에서 볼 수 있듯 미분시 연쇄법칙(chain rule)을 적용합니다.