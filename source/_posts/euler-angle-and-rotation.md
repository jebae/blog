---
title: 오일러 각과 회전 (Euler angle and rotation)
tags:
  - 수학
  - 벡터
  - 그래픽스
categories:
  - 수학
  - 벡터
date: 2019-03-30 15:28:17
---


---
1. 2차원 평면에서의 회전을 알아봅니다.

2. 오일러 각에 대해 알아봅니다.

3. 3차원 회전 행렬과 짐벌 락에 대해 알아봅니다.
---

<br>
### 2차원 평면에서의 회전

2차원 평면 좌표에서 단위원 위의 한 점 $(x, y)$ 가 시계 반대방향으로 $\theta$ 만큼 회전해 $(x', y')$ 가 될 때 이를 삼각함수를 이용해 표현할 수 있습니다.

아래 그림은 점 $(x, y)$, $(x', y')$ 와 각도 $\theta$ 를 표현한 것입니다.

<center>
<img src="https://www.dropbox.com/s/4kvhjemfjmmmiem/rotation_1.jpg?raw=1" width="50%">
</center>

$(x', y')$ 을 표현하는 식을 찾기 위해 아래와 같이 보조선을 그어 보겠습니다.

<center>
<img src="https://www.dropbox.com/s/mgprvau9en02d18/rotation_2.jpg?raw=1" width="50%">
</center>

그림을 보면 점 $(x, y)$ 에 대해 $x$ 축에 수선의 발을 내리는 보조선과 수선의 발과 원점을 잇는 보조선이 있습니다. 이들 보조선과 점 $(x, y)$ 를 이용해 직각삼각형을 만들 수 있고 이를 $\theta$ 만큼 회전한 삼각형도 그릴 수 있습니다. 이 때 점 $(x, y)$ 와 원점을 잇는 직선이 회전한 각도는 보조선이 회전한 각도와 같습니다.

다음으로 직각삼각형 안에 작은 직각삼각형을 그리는 새로운 보조선을 그려 보겠습니다.

<center>
<img src="https://www.dropbox.com/s/sywooq7axhm8npd/rotation_3.jpg?raw=1" width="50%">
</center>

회전된 직각삼각형 안의 작은 직각삼각형에 대해 같은 크기의 원점을 포함한 직각삼각형을 그릴 수 있습니다. 이 때 두 작은 직각삼각형의 빗변은 평행하므로 회전된 보조선과 원점을 포함한 직각삼각형의 빗변이 직각을 이룹니다. 이 사실을 이용해 작은 직각삼각형의 각도를 구할 수 있습니다.

<center>
<img src="https://www.dropbox.com/s/1y0iszwag3urqbj/rotation_4.jpg?raw=1" width="50%">
</center>

그림에서 작은 직각삼각형의 두 모서리 중 큰 각은 $180^\circ - 90^\circ - \theta = 90^\circ - \theta$ 이고 나머지 한 모서리의 각은 $\theta$ 입니다. 작은 직각삼각형의 빗변의 길이가 $y$ 임을 이용해 아래 그림과 같이 $x'$ 를 구할 수 있습니다.

<center>
<img src="https://www.dropbox.com/s/7q2jngjia5yiwdp/rotation_5.jpg?raw=1" width="50%">
</center>

$y'$ 역시 회전한 보조선과 작은 직각삼각형을 이용해 찾을 수 있습니다.

<div><img src="https://www.dropbox.com/s/xqge6knl8gbnwvv/rotation_6.jpg?raw=1" width="50%"><img src="https://www.dropbox.com/s/ro4zreprr8fq98h/rotation_7.jpg?raw=1" width="50%"></div>

$x' = x cos\theta - y sin\theta$, $y' = x sin\theta + y cos\theta$ 를 행렬-벡터 곱셈으로 다음과 같이 쓸 수 있습니다.

$$
\begin{bmatrix}
cos\theta & -sin\theta \\\
sin\theta & cos\theta
\end{bmatrix}
\begin{bmatrix}
x \\\
y
\end{bmatrix}
=
\begin{bmatrix}
x cos\theta - y sin\theta \\\
x sin\theta + y cos\theta
\end{bmatrix}
$$

<br><br>
### 오일러 각

오일러는 복소수를 사용해 편각을 표현하는 다음과 같은 공식을 발견했습니다.

$$
e^{\theta i} = cos \theta + i\text{ }sin\theta
$$

이를 아래 그림과 같이 복소필드상에 그려볼 수 있습니다.

<center>
<img src="https://www.dropbox.com/s/lpbi4mtw28kxzcc/euler_rotation.png?raw=1" width="50%">
<span class="image-caption">출처 : 위키피디아</span>
</center>

복소필드상의 원점 중심의 원 위의 한 점 $re^{\phi i}$을 $\theta$ 만큼 회전시키는 것은 아래 식과 같이 쓸 수 있습니다. 여기서 $r$은 복소필드상의 원점을 중심으로 한 원의 반지름입니다.

$$
re^{\phi i} \cdot e^{\theta i}
$$

예를 들어 단위원위의 점 $z = e^{\frac{\pi}{6} i}$ 를 $\frac{\pi}{3}$ 만큼 회전시키기 위해서는 $e^{\frac{\pi}{3} i}$ 를 $z$ 에 곱해줍니다.

$$
ze^{\frac{\pi}{3} i} = e^{\frac{\pi}{6} i + \frac{\pi}{3} i} = e^{\frac{\pi}{2} i} = cos\frac{\pi}{2} + i \text{ }sin\frac{\pi}{2} = i
$$

이는 실수 평면상의 점 $(cos\frac{\pi}{6}, sin\frac{\pi}{6})$ 을 $\frac{\pi}{3}$ 만큼 회전시켜 $(0, 1)$을 얻는 것과 같습니다.

<br><br>
이렇게 유용한 오일러 각은 여러 방법으로 증명할 수 있습니다. 이 글에서는 미적분을 이용해 증명해 보겠습니다.

$$
z = cos\theta + i \text{ }sin\theta
$$

$z$ 를 $\theta$ 에 대해 미분합니다.

$$
\begin{align}
\frac{dz}{d\theta} & = -sin\theta + i \text{ } cos\theta \\\
& = i(i \text{ } sin\theta + cos\theta) \\\
& = zi
\end{align}
$$

$\frac{dz}{d\theta} = zi$ 는 $\frac{1}{z}\frac{dz}{d\theta} = i$ 로 변형가능합니다. 변형한 식을 적분해보겠습니다.

$$
\int \frac{1}{z} dz = \int i d\theta \\\ \\\
ln \text{ }z = \theta i \\\ \\\ \\\
\therefore z = e^{\theta i}
$$

<br><br>
### 회전 행렬

앞서 살펴 봤던 2차원 회전 행렬 $\begin{bmatrix}
cos\theta & -sin\theta \\\
sin\theta & cos\theta
\end{bmatrix}$ 을 3차원에 적용해 보겠습니다. 2차원에서는 원점에 대해 회전하는 변환이었다면 3차원에서는 축을 기준으로 회전합니다. $x$, $y$, $z$ 축에 대한 각각의 회전은 아래 그림과 같이 그려볼 수 있습니다.

<div><img src="https://www.dropbox.com/s/19fgx81bxelibnf/axis_1.jpg?raw=1" width="30%"><img src="https://www.dropbox.com/s/xksmrdn03tmwfgd/axis_3.jpg?raw=1" width="30%"><img src="https://www.dropbox.com/s/fmo895869u7353x/axis_2.jpg?raw=1" width="30%">
</div>

$z$ 축에 대한 회전은 1사분면만을 기준으로 할 때 $x$ 축에서 $y$ 축 방향으로 회전합니다. $x$ 축에 대한 회전은 $y$ 축에서 $z$ 축 방향으로 회전합니다. $y$ 축에 대한 회전은 $z$ 축에서 $x$ 축 방향으로 회전합니다. 이해를 돕기 위해 $x$, $y$, $z$ 를 각각의 알파벳 순서로 생각한다면 $y$ 축에 대한 회전만이 그 순서가 반대입니다. 3차원 회전 행렬의 행의 순서를 구성하는데 이를 고려해야 합니다. 아래는 $x$ 축, $y$ 축, $z$ 축에 대한 각각의 3차원 회전 행렬입니다.

\begin{bmatrix}
1 & 0 & 0 \\\
0 & cos\theta & -sin\theta \\\
0 & sin\theta & cos\theta
\end{bmatrix}

\begin{bmatrix}
cos\theta & 0 & sin\theta \\\
0 & 1 & 0 \\\
-sin\theta & 0 & cos\theta
\end{bmatrix}

\begin{bmatrix}
cos\theta & -sin\theta & 0 \\\
sin\theta & cos\theta & 0 \\\
0 & 0 & 1
\end{bmatrix}

<br><br>
### 짐벌 락 (Gimbal lock)

위에서 살펴본 3차원 행렬은 짐벌 락이라는 단점을 갖고 있습니다. 짐벌 락은 한축이 다른 축과 겹쳐지면서 축의 자유도를 잃게되는 것을 말합니다. 아래 영상은 3차원 회전 행렬이 실제 어떤 결과를 나타내는지 그리고 짐벌 락이 어떤 상황에 발생하는지를 보여줍니다.

<div class="video-responsive">
  <iframe width="640" height="480" src="http://www.youtube.com/embed/zc8b2Jo7mno" frameborder="0" allowfullscreen></iframe>
</div>
