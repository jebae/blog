---
title: 벡터의 외적과 삼중곱
tags:
  - 수학
  - 벡터
categories:
  - 수학
  - 벡터
date: 2019-03-30 15:28:00
---


<br>
### 외적 Cross product

두 벡터 $a = \langle a_1, a_2, a_3 \rangle$와 $b = \langle b_1, b_2, b_3 \rangle$의 외적은 $a \times b$ 라 쓰고 다음과 같이 정의합니다.

$$
\begin{align}
a \times b & = \hat{n} \parallel a\parallel  \parallel b\parallel sin\theta \\\
& = \langle a_2 b_3 - a_3 b_2,\text{ } a_3 b_1 - a_1 b_3,\text{ } a_1 b_2 - a_2 b_1 \rangle
\end{align}
$$

여기서 $\hat{n}$은 벡터 $a$와 $b$에 모두 수직인 단위벡터입니다. 이 때 두 벡터에 수직인 벡터는 아래 그림과 같이 두 개가 존재하는데 이는 두 벡터의 곱 순서에 따라 정해집니다.

<center>
<img src="https://www.dropbox.com/s/0sdlzv4uqq0tjok/cross_product.png?raw=1" width="30%">
<span class="image-caption">출처 : 위키피디아</span>
</center>

두 벡터에 수직인 벡터의 방향은 오른손 법칙에 따라 정해집니다.

식 $a \times b = \hat{n} \parallel a\parallel  \parallel b\parallel  sin\theta$에서 $\hat{n}$ 이 벡터의 방향을 결정했다면 $\parallel a\parallel  \parallel b\parallel  sin\theta$는 벡터의 크기를 결정합니다. $a \times b = \langle a_2 b_3 - a_3 b_2,\text{ } a_3 b_1 - a_1 b_3,\text{ } a_1 b_2 - a_2 b_1 \rangle$ 임을 이용해 이를 증명해 보겠습니다.

$$
\begin{align}
\parallel a \times b \parallel^2 & = (a_2 b_3 - a_3 b_2)^2 + (a_3 b_1 - a_1 b_3)^2 + (a_1 b_2 - a_2 b_1)^2 \\\ \\\
& = a_2^2 b_3^2 - 2 a_2 a_3 b_2 b_3 + a_3^2 b_2^2 \\\
& + a_3^2 b_1^2 - 2 a_1 a_3 b_1 b_3 + a_1^2 b_3^2 \\\
& + a_1^2 b_2^2 - 2 a_1 a_2 b_1 b_2 + a_2^2 b_1^2 \\\ \\\
& = a_2^2(b_1^2 + b_2^2 + b_3^2 - b_2^2) \\\
& + a_3^2(b_1^2 + b_2^2 + b_3^2 - b_3^2) \\\
& + a_1^2(b_1^2 + b_2^2 + b_3^2 - b_1^2) \\\
& -2(a_2 a_3 b_2 b_3 + a_1 a_3 b_1 b_3 + a_1 a_2 b_1 b_2) \\\ \\\
& = (a \cdot a)(b \cdot b) - (a \cdot b)^2 \\\
& = \parallel a \parallel^2 \parallel b \parallel^2 - (\parallel a \parallel \parallel b \parallel cos \theta)^2 \\\
& = \parallel a \parallel^2 \parallel b \parallel^2 sin^2 \theta \\\ \\\
& \therefore \parallel a \times b \parallel = \parallel a \parallel \parallel b \parallel sin \theta 
\end{align}
$$

<br><br>
외적의 몇가지 성질을 알아보겠습니다.
- 교환법칙이 성립하지 않습니다.
그림에서 보았듯 $a \times b$와 $b \times a$는 서로 반대방향이므로 $a \times b = - b \times a$입니다.
- 스칼라 곱에 대해 선형적인 성질을 가집니다.
$(\alpha a) \times b = a \times (\alpha b) = \alpha (a \times b)$
- 벡터 덧셈에 대해 분배법칙이 성립합니다.
$a \times (b + c) = a \times b + a \times c$

한편 외적의 정의 $a \times b = \langle a_2 b_3 - a_3 b_2,\text{ } a_3 b_1 - a_1 b_3,\text{ } a_1 b_2 - a_2 b_1 \rangle$ 은 행렬식으로써 표현할 수도 있습니다.

$$
a \times b = 
\begin{vmatrix}
1 & 1 & 1 \\\
a_1 & a_2 & a_3 \\\
b_1 & b_2 & b_3
\end{vmatrix}
$$

<br><br>
### 스칼라 삼중곱

스칼라 삼중곱이란 두 개의 벡터의 외적을 다른 한 벡터와 내적하는 형태를 말합니다.

$$
a \cdot (b \times c)
$$

스칼라 삼중곱은 아래와 같이 세 개의 벡터로 이루어지는 평행육면체의 부피를 나타냅니다.

<center>
<img src="https://www.dropbox.com/s/yirea70cpaalghe/scalar_triple_product.png?raw=1" width="40%">
<span class="image-caption">출처 : 위키피디아</span>
</center>

$b \times c = \parallel b \parallel \parallel c \parallel sin \theta$ 이고, 이는 벡터 $b$와 $c$가 이루는 평면의 넓이를 뜻합니다. 그리고 식 $a \cdot (b \times c)$ 은 $\parallel a \parallel \parallel b \times c \parallel cos\text{ }\alpha$ 로 다시 쓸 수 있고 이는 $b$와 $c$가 이루는 평면의 넓이와 평면에 대해 수직인 $a \text{ }cos \text{ } \alpha$ 의 곱, 즉 평행육면체의 부피를 나타냅니다.

벡터의 외적의 순서를 고려해봤을 때, $a \cdot (b \times c) = b \cdot (c \times a) = c \cdot (a \times b)$ 이 성립함을 오른손법칙을 이용해 알 수 있습니다.

스칼라 삼중곱은 두 벡터의 외적의 결과와 한 벡터의 내적이므로 행렬식을 이용해 다음과 같이 쓸 수 있습니다.

$$
a \cdot (b \times c) =
\begin{vmatrix}
a_1 & a_2 & a_3 \\\
b_1 & b_2 & b_3 \\\
c_1 & c_2 & c_3
\end{vmatrix}
$$

<br><br>
### 벡터 삼중곱

벡터 삼중곱은 두 벡터의 외적에 대해 다른 한 벡터와 다시 외적하는 형태를 말합니다.

$$
a \times (b \times c)
$$

$a \times (b \times c) = b(a \cdot c) - c(a \cdot b)$ 를 만족합니다. 이는 $a \times (b \times c)$ 를 직접 전개해 얻을 수 있습니다.

$$
\begin{align}
a \times (b \times c) & = a_2(b_1 c_2 - b_2 c_1) - a_3(b_3 c_1 - b_1 c_3) \\\
& + a_3(b_2 c_3 - b_3 c_2) - a_1(b_1 c_2 - b_2 c_1) \\\
& + a_1(b_3 c_1 - b_1 c_3) - a_2(b_2 c_3 - b_3 c_2) \\\ \\\
& = a_2 b_1 c_2 - a_2 b_2 c_1 - a_3 b_3 c_1 + a_3 b_1 c_3 \\\
& + a_3 b_2 c_3 - a_3 b_3 c_2 - a_1 b_1 c_2 + a_1 b_2 c_1 \\\
& + a_1 b_3 c_1 - a_1 b_1 c_3 - a_2 b_2 c_3 + a_2 b_3 c_2 \\\ \\\
& = b_1(a_2 c_2 + a_3 c_3) - c_1(a_2 b_2 + a_3 b_3) \\\
& + b_2(a_3 c_3 + a_1 c_1) - c_2(a_3 b_3 + a_1 b_1) \\\
& + b_3(a_1 c_1 + a_2 c_2) - c_3(a_1 b_1 + a_2 b_2) \\\ \\\
& = b_1(a_2 c_2 + a_3 c_3 + a_1 c_1) - c_1(a_2 b_2 + a_3 b_3 + a_1 c_1) \\\
& + b_2(a_3 c_3 + a_1 c_1 + a_2 c_2) - c_2(a_3 b_3 + a_1 b_1 + a_2 c_2) \\\
& + b_3(a_1 c_1 + a_2 c_2 + a_3 c_3) - c_3(a_1 b_1 + a_2 b_2 + a_3 c_3) \\\ \\\
& = b_1(a \cdot c) - c_1(a \cdot b) \\\
& + b_2(a \cdot c) - c_2(a \cdot b) \\\
& + b_3(a \cdot c) - c_3(a \cdot b) \\\ \\\
& = b(a \cdot c) - c(a \cdot b)
\end{align}
$$