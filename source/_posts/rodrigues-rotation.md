---
title: 로드리게스 회전 (Rodrigues rotation)
tags:
  - 수학
  - 벡터
  - 그래픽스
categories:
  - 수학
  - 벡터
date: 2019-03-30 15:28:27
---


<br>
### 로드리게스 회전에 대한 기하학적 이해

로드리게스 회전은 3차원 공간상의 물체의 회전을 회전축과 그 각도로 설명합니다. 아래 그림은 단위 벡터인 회전축 $n$ 과 벡터 $v$, 그리고 $v$ 가 $n$ 에 대해 $\theta$ 만큼 회전한 벡터 $v'$ 를 보여주고 있습니다.

<center>
<img src="https://www.dropbox.com/s/7fgj1iq94lrw7fv/rodrigues_rotation_1.jpg?raw=1" width="30%">
</center>

회전된 벡터 $v'$ 는 아래와 같이 두가지 성분으로 분해할 수 있습니다.

<center>
<img src="https://www.dropbox.com/s/ywqwfq0w88gxh7r/rodrigues_rotation_2.jpg?raw=1" width="30%">
</center>

식 $v' = v'^{\parallel n} + v'^{\perp n}$ 에서 $v'^{\parallel n}$ 는 회전하기 이전의 벡터 $v$ 의 $n$ 에 대한 투영인 $v^{\parallel n}$ 와 같습니다. 따라서 $v' = v^{\parallel n} + v'^{\perp n}$ 입니다.

$v'$ 의 $n$ 에 대한 직교 투영인 $v'^{\perp n}$ 을 구하기 위해 아래 그림을 살펴보겠습니다.

<center>
<img src="https://www.dropbox.com/s/e9l9ag9fy5g6ffp/rodrigues_rotation_3.jpg?raw=1" width="50%">
</center>

벡터 $v'^{\perp n}$ 은 두가지 성분으로 분해할 수 있습니다. $\parallel v'^{\perp n} \parallel = \parallel v^{\perp n} \parallel$ 이고 $v'^{\perp n}$ 의 가로 성분은 아래와 같이 쓸 수 있습니다.

$$
\begin{align}
v'^{\perp n}_{horizontal} & = \parallel v^{\perp n} \parallel cos\theta \cdot \frac{v^{\perp n}}{\parallel v^{\perp n} \parallel} \\\
& = v^{\perp n} cos\theta
\end{align}
$$

세로 성분을 구하기 전에 $n \times v$ 를 생각해 보겠습니다. $n \times v$ 의 방향은 $v^{\perp n}$ 에 수직이고 $\parallel n \times v \parallel = \parallel n \parallel \parallel v \parallel sin \phi = \parallel v \parallel sin \phi$ 이므로 $v^{\perp n}$ 와 크기가 같습니다.

$\parallel v'^{\perp n} \parallel = \parallel n \times v \parallel$ 이고 세로 성분은 아래와 같이 쓸 수 있습니다.

$$
\begin{align}
v'^{\perp n}_{vertical} & = \parallel n \times v \parallel cos(90 - \theta) \cdot \frac{n \times v}{\parallel n \times v \parallel} \\\
& = (n \times v) sin\theta
\end{align}
$$

$v'^{\perp n}$ 의 가로와 세로 성분을 이용해 다음과 같이 쓸 수 있습니다.

$$
v'^{\perp n} = v^{\perp n} cos\theta + (n \times v) sin\theta
$$

종합하면 $v' = v^{\parallel n} + v^{\perp n} cos\theta + (n \times v) sin\theta$ 이고 이를 다음과 같이 정리할 수 있습니다.

$$
\begin{align}
v' & = v^{\parallel n} + v^{\perp n} cos\theta + (n \times v) sin\theta \\\
& = v^{\parallel n} + (v - v^{\parallel n}) cos\theta + (n \times v) sin\theta \\\
& = (1 - cos\theta)v^{\parallel n} + v cos\theta + (n \times v) sin\theta \\\
& = (1 - cos\theta)\frac{v \cdot n}{n \cdot n}n + v cos\theta + (n \times v) sin\theta \\\
& = (1 - cos\theta)(v \cdot n)n + v cos\theta + (n \times v) sin\theta \\\
\end{align}
$$

<br><br>
### 로드리게스 회전 행렬

로드리게스 회전 식 $v' = (1 - cos\theta)(v \cdot n)n + v cos\theta + (n \times v) sin\theta$ 을 이용해 회전 행렬을 구해보겠습니다. 회전 식은 아래와 같이 다시 쓸 수 있습니다.

$$
v' = (1 - cos\theta)nn^Tv + cos\theta \text{ }v + sin\theta \text{ }(n \times v) \\\
$$

먼저 $n \times v$ 을 행렬-벡터 곱셈으로서 표현해 보겠습니다.

$$
n \times v = \begin{bmatrix}
n_y v_z - n_z v_y \\\
n_z v_x - n_x v_z \\\
n_x v_y - n_y v_x
\end{bmatrix}
= \begin{bmatrix}
0 & -n_z & n_y \\\
n_z & 0 & -n_x \\\
-n_y & n_x & 0
\end{bmatrix}
\begin{bmatrix}
v_x \\\
v_y \\\
v_z
\end{bmatrix}
$$

위 식에서 행렬 $\begin{bmatrix}
0 & -n_z & n_y \\\
n_z & 0 & -n_x \\\
-n_y & n_x & 0
\end{bmatrix}$ 을 $K$ 라 하고 $K^2$ 을 구해보겠습니다.

$$
K^2 = \begin{bmatrix}
0 & -n_z & n_y \\\
n_z & 0 & -n_x \\\
-n_y & n_x & 0
\end{bmatrix}
\begin{bmatrix}
0 & -n_z & n_y \\\
n_z & 0 & -n_x \\\
-n_y & n_x & 0
\end{bmatrix}
=
\begin{bmatrix}
-n_z^2 - n_y^2 & n_x n_y & n_z n_x \\\
n_x n_y & -n_z^2 - n_x^2 & n_y n_z \\\
n_x n_z & n_y n_z & -n_x^2 - n_y^2
\end{bmatrix}
$$

$K^2$ 의 형태를 자세히 보면 다음과 같이 다시 쓸 수 있습니다.

$$
K^2 =
\begin{bmatrix}
n_x^2 - 1 & n_x n_y & n_z n_x \\\
n_x n_y & n_y^2 - 1 & n_y n_z \\\
n_x n_z & n_y n_z & n_z^2 - 1
\end{bmatrix} =
nn^T - I \\\ \\\
(\because n_x^2 + n_y^2 + n_z^2 = 1)
$$

위 식에 의하면 $nn^T = K^2 + I$ 입니다.

$nn^T = K^2 + I$ 와 $n \times v = Kv$ 을 이용해 $v' = (1 - cos\theta)nn^Tv + cos\theta \text{ }v + sin\theta \text{ }(n \times v)$ 을 다시 정리해보겠습니다.

$$
\begin{align}
v' & = (1 - cos\theta)nn^Tv + cos\theta \text{ }v + sin\theta \text{ }(n \times v) \\\
& = ((1 - cos\theta)(K^2 + I) + cos\theta \text{ }I + sin\theta \text{ }K)v
\end{align}
$$

따라서 $v$ 를 회전축 $n$ 에 대해 $\theta$ 만큼 회전시키는 것은 $v$ 에 아래 행렬을 곱하는 것과 같습니다.

$$
(1 - cos\theta)(K^2 + I) + cos\theta \text{ }I + sin\theta \text{ }K \\\
= I + (1 - cos\theta)K^2 + sin\theta \text{ }K
$$



<br><br><br>
참고 : https://www.youtube.com/watch?v=q-ESzg03mQc
참고 : http://web.eecs.umich.edu/~sugih/courses/eecs487/common/notes/rotations.pdf