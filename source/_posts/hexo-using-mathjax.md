---
title: hexo 에서 mathjax 사용하기
tags:
  - hexo
  - mathjax
  - javascript
categories:
  - javascript
  - hexo
date: 2018-11-12 22:36:57
---


`hexo`에서 `mathjax` 를 이용하려 구글 검색을 해보면 매뉴얼을 그대로 따라도 안되는 경우가 많습니다. `hexo`가 중국에서 개발된 소스이다보니 관련 문서도 대부분 중국어로 쓰여 있어 읽기가 쉽지 않습니다.

<br><br>
### *hexo-math*는 버릴 것

`hexo`와 `mathjax`를 함께 검색 하면 구글의 경우 가장 상단에 `hexo-math` 라이브러리가 나옵니다. 작성일 기준(2018-11-12) 한 달 전에도 커밋이 올라올 만큼 업데이트가 활발해 보였지만 그에 못지 않게 `hexo-math`의 사용법을 그대로 따랐음에도 적용이 되지 않는 이슈를 블로깅한 글들이 많습니다. 중국어로 된 대부분의 문서에서는 다음 순서를 따라보도록 권고하고 있습니다.

<br><br>
### *hexo-renderer-kramed* 설치

```
npm uninstall hexo-renderer-marked --save
npm install hexo-renderer-kramed --save

```

<br><br>
### *renderer.js* 수정

`/node_modules/hexo-renderer-kramed/lib/renderer.js` 를 열고

```javascript
function formatText(text) {
    // Fit kramed's rule: $$ + \1 + $$
    return text.replace(/`\$(.*?)\$`/g, '$$$$$1$$$$');
}
```
위의 코드를 아래처럼 바꿉니다.
```javascript
function formatText(text) {
      return text;
}
```

<br><br>
### *hexo-renderer-mathjax* 설치

```
npm uninstall hexo-math --save
npm install hexo-renderer-mathjax --save
```

<br><br>
### *mathjax.html* 파일 *script* 태그의 *src* 수정

`/node_modules/hexo-renderer-mathjax/mathjax.html` 파일을 열어 `<script>`의 `src`를 다음과 같이 수정합니다.

```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML"></script>
```

<br><br>
### *front-matter* 수정

아래와 같이 *front-matter*에 `mathjax: true`를 추가합니다.
```
---
title: <티이틀>
date: 2018/11/12
mathjax: true
---
```

<br><br>
### 다른 방법

대부분의 문서에서 찾을 수 있는 위의 방법을 따라도 좋지만 보다 간단한 방법으로 해결할 수도 있었습니다.

- `hexo-renderer-kramed` 로 바꿀 필요는 없습니다.
- `_config.yml` 도 수정할 필요 없습니다.
- *front-matter* 에 `mathjax: true` 를 추가할 필요 없습니다.
- `hexo-renderer-mathjax` 설치
- `/node_modules/hexo-renderer-mathjax/mathjax.html` 파일 `<script>` `src`를 다음과 같이 수정합니다.
```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML"></script>
```

`/node_modules/hexo-renderer-mathjax/mathjax.html` 파일 `<script>` `src` 변경의 경우 바꾸지 않아도 로컬에서는 정상적인 `mathjax`로 작성된 수학식을 볼 수 있습니다. 하지만 배포시 개발자도구를 켜면 `mathjax`의 *deprecated* 경고와 함께 `$` 표시만 가득한 페이지를 보게 됩니다. 관련 문서는 링크를 참조하셔도 좋습니다.(*https://www.mathjax.org/cdn-shutting-down/* )

<br><br><br>
참고: https://nathaniel.blog/tutorials/make-hexo-support-math-again/