---
title: 방향 도함수와 Gradient 벡터
tags:
  - 수학
  - 미적분
categories:
  - 수학
  - 미적분
date: 2019-02-25 11:40:01
---

### 방향 도함수 (Directional derivative)

미분계수 또는 도함수는 기울기를 뜻합니다. 2차원 평면에서 $x$ 의 아주 작은 변화량에 대해 $y$ 의 기울기를 구하면 그것을 도함수라 불렀었습니다. 방향 도함수는 벡터에 대한 도함수라 생각할 수 있습니다. 편미분이 $x$ 축과 $y$ 축에 대해 변화율을 구하는 수단이었다면 방향 도함수는 임의의 방향에 대해 그 변화율을 구할 수 있게 해줍니다. 아래 그림은 3차원 좌표계에서 단위 벡터 $v$ 에 대해 $z = f(x, y)$ 의 한 점 $P(x_0, y_0)$ 에서의 도함수를 구하는 과정을 요약적으로 보여줍니다.

<center>
<img src="https://www.dropbox.com/s/k689ern7clzkxlu/arc.jpg?raw=1" width="70%">
</center>

$v = (v_1, v_2)$ 일 때 $z$ 의 기울기 즉, 방향도함수를 다음과 같이 쓸 수 있습니다.

$$
lim_{t \to 0}\frac{f(x_0 + t v_1, y_0 + t v_2) - f(x_0, y_0)}{t}
$$

한편 이 식은 잠시후 알아볼 Gradient 벡터 $\nabla f$ 와 단위 벡터 $v$ 의 내적 $\nabla f \cdot v$ 와 같습니다. 이를 증명하기 위해 식 $g(t) = f(x_0 + t v_1, y_0 + t v_2)$ 를 정의하겠습니다.

$$
\begin{align}
g'(t) & = lim_{h \to 0} \frac{g(t + h) - g(t)}{h} \\\ \\\
g'(0) & = lim_{h \to 0} \frac{g(h) - g(0)}{h} \\\ \\\
& = lim_{h \to 0}\frac{f(x_0 + h v_1, y_0 + h v_2) - f(x_0, y_0)}{h}
\end{align}
$$

위 식은 $g'(0)$ 이 $z = f(x, y)$ 의 방향도함수와 일치함을 보여줍니다. 한편 $g'(t)$ 를 아래와 같이 쓸 수 있습니다.

$$
\begin{align}
g'(t) & = \frac{\partial g}{\partial x} \frac{dx}{dt} + \frac{\partial g}{\partial y} \frac{dy}{dt} \\\ \\\
& = \frac{\partial g}{\partial x} \frac{d(x_0 + t v_1)}{dt} + \frac{\partial g}{\partial y} \frac{d(y_0 + t v_2)}{dt} \\\ \\\ \\\
g'(0) & = \frac{\partial f(x_0, y_0)}{\partial x} v_1 + \frac{\partial f(x_0, y_0)}{\partial y} v_2 \\\ \\\
& = \nabla f \cdot v
\end{align}
$$

따라서 식 $z = f(x, y)$ 의 방향도함수는 $\nabla f \cdot v$ 와 같습니다. 단위벡터 $v$ 에 대한 $f$ 의  방향도함수는 $D_v f$ 라 쓰고 아래와 같이 쓸 수 있습니다.

$$
D_v f = \nabla f \cdot v
$$

<br><br>
### Gradient 벡터

Gradient는 기울기를 뜻합니다. 기울기는 어느 시간 또는 지점에서의 증가율을 말합니다. 함수 $f$의 Gradient는 $\nabla f$과 같이 쓰고 삼변수 $x, y, z$에 대해 다음을 $f$의 Gradient 벡터라 정의합니다.

$$
\nabla f = \frac{\partial f}{\partial x} i + \frac{\partial f}{\partial y} j + \frac{\partial f}{\partial z} k
$$

이 때 $i, j, k$는 $x$축, $y$축, $z$축에 대한 단위방향벡터입니다. Gradient 벡터의 공식은 편미분(partial derivative) 후 각 벡터를 더하는 형태를 가집니다. 이 때 미분은 순간 변화량 즉 기울기를 뜻하며 스칼라값이지만 Gradient 벡터는 어느 지점에서의 기울기를 나타내는 **벡터**입니다.

한편 Gradient 벡터는 함수의 어느 지점에서 기울기가 가장 큰 벡터이기도 합니다. 이를 단위방향벡터 $v$에 대한 함수 $f(x, y)$ 의 방향도함수로 증명해 보겠습니다. 방향도함수는 어느 지점에서의 임의의 방향에 대한 기울기이므로 이 기울기가 최대가 될 때를 고려하면 됩니다.

$$
\begin{align}
\nabla_v f & = \nabla f \cdot v \\\
& = \parallel \nabla f \parallel \parallel v \parallel cos \theta
\end{align}
$$

위 식에서 $\theta$는 벡터 $v$와 Gradient 벡터가 이루는 각도를 의미하고 $cos \theta$가 최댓값 1일 때 방향도함수는 최댓값 $\parallel \nabla f \parallel$를 가집니다. 따라서 Gradient 벡터는 어느 지점에서 기울기가 가장 큰 벡터입니다. 한편 위 식에서 $cos \theta = 1$ 은 벡터 $v$ 가 함수 $f$ 의 한 지점에서 기울기가 가장 큰 방향의 단위 벡터라는 것을 의미하기도 합니다.

아래 그림은 함수 $xe^{-(x^2 + y^2)}$을 그린 것이고 그림에서 볼 수 있는 화살표는 각 지점에서의 Gradient 벡터라 할 수 있습니다.

<center>
<img src="https://www.dropbox.com/s/idnalua2h03w9an/Gradient_of_a_Function.jpg?raw=1" width="50%">
<span class="image-caption">출처 : 위키피디아</span>
</center>

그림의 화살표를 따라가는 것은 기울기가 가장 큰 Gradient를 따라가는 것이고 이는 함수의 최댓값을 찾아가는 것과 같습니다. Gradient 벡터는 이렇게 최대점을 찾는데 쓸 수 있습니다. 

Gradient 벡터는 접선 벡터 또는 접평면 벡터의 법선벡터이기도 합니다. 공간상에 $f(x,y,z) = k$라는 곡면이 있을 때 곡면의 한 지점을 $P(x_0, y_0, z_0)$라 하고 $P$를 지나면서 곡면의 일부인 곡선을 $r(t) = \langle x(t), y(t), z(t) \rangle$라 하겠습니다. 곡선 $r(t)$는 곡면의 일부이므로 $f(x(t), y(t), z(t)) = k$가 성립합니다. 이 식을 $t$에 대해 전미분하면 아래와 같이 쓸 수 있습니다.

$$
\frac{\partial f}{\partial x} \frac{dx}{dt} + \frac{\partial f}{\partial y} \frac{dy}{dt} + \frac{\partial f}{\partial z} \frac{dz}{dt} = 0
$$

이는 Gradient $\nabla f$와 $r(t)$의 도함수(접평면 벡터) $r^\prime(t)$ 내적입니다.

$$
\nabla f \cdot r^\prime(t) = 0
$$

따라서 접선 벡터 또는 접평면 벡터에 대해 Gradient 벡터는 법선벡터입니다.