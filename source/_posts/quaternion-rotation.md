---
title: 쿼터니언 회전 (Quaternion rotation)
tags:
  - 수학
  - 벡터
  - 그래픽스
  - 복소수
  - 사원수
  - Quaternion
categories:
  - 수학
  - 벡터
date: 2019-03-30 15:28:36
---


**이 글은 {% post_link rodrigues-rotation 로드리게스 회전 %} 을 이해하고 접하시길 추천드립니다.*

---
1. 사원수에 대해 알아봅니다.

2. 사원수와 오일러 각의 관계를 알아봅니다.

3. 쿼터니언 회전과 그 행렬에 대해 알아봅니다.
---

<br>
### 사원수 Quaternion

사원수는 아일랜드의 수학자 윌리엄 해밀턴(William Rowan Hamilton)이 복소수를 확장해 만든 수체계입니다. 복소수를 일반적으로 $a + bi$로 쓸 수 있다면 사원수는 $a + bi + cj + dk$ 와 같이 쓸 수 있습니다. 이 때 $a$ 는 실수부, $bi + cj + dk$ 는 허수부 또는 벡터부라 합니다. 사원수는 아래와 같은 매우 짧은 식으로부터 정의됩니다.

$$
i^2 = j^2 = k^2 = ijk = -1
$$

$ijk = -1$ 의 양변에 $i$, $j$, $k$ 를 곱하며 다음과 같은 식들을 이끌어낼 수 있습니다.

$$
ij = k \\\
ji = -k \\\
jk = i \\\
kj = -i \\\
ki = j \\\
ik = -j \\\
$$

위 식에서 알 수 있는 중요한 사실은 사원수의 곱셈에 교환법칙이 성립하지 않는다는 것입니다. 

<br><br>
두 사원수 $w = w_0 + w_1i + w_2j + w_3k$ 와 $z = z_0 + z_1i + z_2j + z_3k$ 에 대해 곱셈 $wz$ 는 다음과 같습니다.

$$
wz = (s_w, v_w)(s_z, v_z) = s_w s_z - v_w \cdot v_z + s_w v_z + s_z v_w + v_w \times v_z \\\
$$

이 때 $s_x$ 는 실수부 값을, $v_x$ 는 벡터부를 의미합니다. 다소 복잡해 보이지만 $wz$ 를 직접 전개하면 얻을 수 있습니다.

$$
\begin{align}
wz & = (w_0 + w_1i + w_2j + w_3k)(z_0 + z_1i + z_2j + z_3k) \\\
& = w_0 z_0 - (w_1 z_1 + w_2 z_2 + w_3 z_3) \\\
& + (w_0 z_1 + w_1 z_0 + w_2 z_3 - w_3 z_2)i \\\
& + (w_0 z_2 + w_2 z_0 + w_3 z_1 - w_1 z_3)j \\\
& + (w_0 z_3 + w_3 z_0 + w_1 z_2 - w_2 z_1)k \\\ \\\
& = w_0 z_0 - (w \cdot z) + w_0 \begin{bmatrix}
\text{ } \\\
z \\\
\text{ }
\end{bmatrix} + z_0 \begin{bmatrix}
\text{ } \\\
w \\\
\text{ }
\end{bmatrix} + \begin{bmatrix}
w_2 z_3 - w_3 z_2 \\\
w_3 z_1 - w_1 z_3 \\\
w_1 z_2 - w_2 z_1
\end{bmatrix} \\\
& = w_0 z_0 - (v_w \cdot v_z) + w_0 v_z + z_0 v_w + v_w \times v_z
\end{align}
$$

사원수의 곱셈을 이용해 다음과 같은 간단한 정리도 이끌어 낼 수 있습니다.

$$
\begin{align}
wz - zw & = s_w s_z - v_w \cdot v_z + s_w v_z + s_z v_w + (v_w \times v_z) \\\
& - (s_z s_w - v_z \cdot v_w + s_z v_w + s_w v_z + (v_z \times v_w)) \\\ \\\
& = (v_w \times v_z) - (v_z \times v_w) \\\
& = 2(v_w \times v_z)
\end{align}
$$

위 식은 $[w, z] = wz - zw$ 로 쓰기도 합니다.

<br><br>
사원수의 실수부가 0인 경우를 *pure quaternion* 이라 합니다. 사원수 $w$ 와 $z$ 가 *pure quaternion* 일 때 다음이 성립합니다.

$$
\begin{align}
wz + zw & = s_w s_z - v_w \cdot v_z + s_w v_z + s_z v_w + (v_w \times v_z) \\\
& + s_z s_w - v_z \cdot v_w + s_z v_w + s_w v_z + (v_z \times v_w) \\\ \\\
& = -2(v_w \cdot v_z) + (v_w \times v_z) + (v_z \times v_w) (\because s_w, s_z = 0)\\\
& = -2(v_w \cdot v_z) + (v_w \times v_z) - (v_w \times v_z) \\\
& = -2(v_w \cdot v_z)
\end{align}
$$

<br><br>
복소수에 대응하는 켤레 복소수를 곱하면 복소수의 크기의 제곱을 얻습니다. 이는 복소수 $z = a + bi$ 에 대해 다음과 같은 식이 성립함을 의미합니다.

$$
zz^{\*} = (a + bi)(a - bi) = a^2 + b^2 = \parallel z \parallel ^2 \\\
(z^{\*}는 \text{ }z의 \text{ }켤레복소수)
$$

사원수에 대해서도 켤레 사원수(conjugate quaternion)를 정의합니다. 사원수 $q = q_0 + q_1i + q_2j + q_3k$ 에 대해 다음이 성립합니다.

$$
\begin{align}
qq^{\*} & = (q_0 + q_1i + q_2j + q_3k)(q_0 - q_1i - q_2j - q_3k) \\\
& = q_0^2 + q_1^2 + q_2^2 + q_3^2 \\\
& = \parallel q \parallel ^2 \\\
& (q^{\*}는 \text{ }q의 \text{ }켤레사원수)
\end{align}
$$

사원수 $q$ 의 크기 $\parallel q \parallel$ 가 1일 때 사원수 $q$ 를 단위 사원수라고 합니다. 단위 사원수에 대해 사원수의 크기에 대한 식 $qq^{\*} = \parallel q \parallel ^2$ 을 이용해 식 $q^{\*} = q^{-1}$ 을 이끌어낼 수 있습니다.

$$
\begin{align}
& qq^{\*} = \parallel q \parallel ^2 \\\
& q^{\*} = \parallel q \parallel ^2 q^{-1} \\\
& q^{\*} = q^{-1}
\end{align}
$$

<br><br>
### 오일러 각과 사원수

$w = w_0 + w_1i + w_2j + w_3k$ 가 단위 사원수라 할 때 $\parallel w \parallel ^2 = w_0^2 + w_1^2 + w_2^2 + w_3^2 = 1$ 이므로 $-1 \le w_0 \le 1$ 입니다. 따라서 $w_0 = cos\theta$ 로 쓸 수 있습니다.
$w = cos\theta + v\text{ }\text{ }(v = w_1i + w_2j + w_3k)$ 라 다시 쓸 때 다음이 성립합니다.

$$
\parallel w \parallel ^2 = cos^2 \theta + \parallel v \parallel ^2 = 1 \\\
\to \parallel v \parallel ^2 = 1 - cos^2 \theta = sin^2 \theta 
$$

$w$ 의 벡터부 $v$ 를 $\parallel v \parallel \hat{u}$ 라 할 때 ($\hat{u}$ 는 $v$ 와 방향이 같은 단위벡터) 다음과 같이 식을 다시 쓸 수 있습니다.

$$
\begin{align}
v & = \parallel v \parallel \hat{u} \\\
& = \hat{u} sin\theta
\end{align}
$$

따라서 $w = cos\theta + v$ 를 다음과 같이 오일러 각에 대한 표현으로 바꿔 쓸 수 있습니다.

$$
w = cos \theta + \hat{u} sin \theta = e^{\theta \hat{u}}
$$

<br><br>
### 쿼터니언 회전

아래 그림은 벡터 회전축 $\hat{n}$ 과 벡터 $v$ 의 $\hat{n}$ 에 대한 회전을 보여주고 있습니다.

<center>
<img src="https://www.dropbox.com/s/vd7dh7t9cqcggzu/quaternion_rotation.jpg?raw=1" width="50%">
</center>

위 그림은 {% post_link rodrigues-rotation 로드리게스 회전 %}을 이용해 아래와 같이 쓸 수 있습니다.

$$
v' = v^{\parallel n} + v'^{\perp n}
$$

$v'^{\perp n}$ 은 오일러의 각을 이용해 아래와 같이 다시 쓸 수 있습니다.

$$
v'^{\perp n} = e^{\theta \hat{n}} v^{\perp n} \\\
\to v' = v^{\parallel n} + e^{\theta \hat{n}} v^{\perp n}
$$

앞으로 보게 될 두 가지 정리는 위 식을 이용해 회전을 사원수의 곱셈으로 표현하는데 이용됩니다. 첫번째 정리는 아래와 같습니다. (식의 가독성을 위해 $v^{\perp \hat{n}}$ 은 $v_{\perp}$ 로, $v^{\parallel \hat{n}}$ 은 $v_{\parallel}$ 로 표기하였습니다.)

$$
e^{\theta \hat{n}} v_{\perp} = v_{\perp} e^{-\theta \hat{n}}
$$

사원수의 곱셈을 이용해 이를 증명할 수 있습니다.

$$
\begin{align}
e^{\theta \hat{n}} v_{\perp} & = (cos\theta, \hat{n}sin\theta)(0, v_{\perp}) \\\ \\\
& = -\hat{n} \cdot v_{\perp} sin\theta + v_{\perp} cos\theta + (\hat{n} \times v_{\perp}) sin\theta \\\ \\\
& = v_{\perp} cos\theta + (\hat{n} \times v_{\perp}) sin\theta \text{ }\text{ }\text{ }(\because \hat{n} \cdot v_{\perp} = 0) \\\ \\\ \\\
v_{\perp} e^{-\theta \hat{n}} & = (0, v_{\perp})(cos\theta, -\hat{n}sin\theta) \\\ \\\
& = - v_{\perp} \cdot (-\hat{n}) sin\theta + v_{\perp} cos\theta - (v_{\perp} \times \hat{n}) sin\theta \\\ \\\
& = v_{\perp} cos\theta + (\hat{n} \times v_{\perp}) sin\theta \text{ }\text{ }\text{ }(\because \hat{n} \cdot v_{\perp} = 0) \\\ \\\ \\\
& \therefore e^{\theta \hat{n}} v_{\perp} = v_{\perp} e^{-\theta \hat{n}}
\end{align}
$$

<br><br>
두번째 정리는 아래와 같습니다.

$$
e^{\theta \hat{n}} v_{\parallel} = v_{\parallel} e^{\theta \hat{n}}
$$

증명은 첫번째 정리와 같이 사원수의 곱셈을 이용합니다.

$$
\begin{align}
e^{\theta \hat{n}} v_{\parallel} & = (cos\theta, \hat{n}sin\theta)(0, v_{\parallel}) \\\ \\\
& = -\hat{n} \cdot v_{\parallel} sin\theta + v_{\parallel} cos\theta + (\hat{n} \times v_{\parallel}) sin\theta \\\ \\\
& = -\hat{n} \cdot v_{\parallel} sin\theta + v_{\parallel} cos\theta\text{ }\text{ }\text{ }(\because \hat{n} \times v_{\parallel} = 0) \\\ \\\ \\\
v_{\parallel} e^{\theta \hat{n}} & = (0, v_{\parallel})(cos\theta, \hat{n}sin\theta) \\\ \\\
& = - v_{\parallel} \cdot \hat{n} sin\theta + v_{\parallel} cos\theta + (v_{\parallel} \times \hat{n}) sin\theta \\\ \\\
& = - v_{\parallel} \cdot \hat{n} sin\theta + v_{\parallel} cos\theta \text{ }\text{ }\text{ }(\because v_{\parallel} \times \hat{n} = 0) \\\ \\\ \\\
& \therefore e^{\theta \hat{n}} v_{\parallel} = v_{\parallel} e^{\theta \hat{n}}
\end{align}
$$

이제 식 $v' = v^{\parallel \hat{n}} + e^{\theta \hat{n}} v^{\perp \hat{n}}$ 을 조금 변경해보겠습니다.

$$
v' = e^{\frac{\theta}{2}\hat{n}} e^{-\frac{\theta}{2}\hat{n}} v^{\parallel \hat{n}} + e^{\frac{\theta}{2}\hat{n}} e^{\frac{\theta}{2}\hat{n}} v^{\perp \hat{n}}
$$

앞서 살펴본 두 개의 정리를 이용해 위 식을 다음과 같이 바꿔 쓸 수 있습니다.

$$
\begin{align}
v' & = e^{\frac{\theta}{2}\hat{n}} v^{\parallel \hat{n}} e^{-\frac{\theta}{2}\hat{n}} + e^{\frac{\theta}{2}\hat{n}} v^{\perp \hat{n}} e^{-\frac{\theta}{2}\hat{n}} \\\
& = e^{\frac{\theta}{2}\hat{n}} (v^{\parallel \hat{n}} + v^{\perp \hat{n}}) e^{-\frac{\theta}{2}\hat{n}} \\\
& = e^{\frac{\theta}{2}\hat{n}} v e^{-\frac{\theta}{2}\hat{n}}
\end{align}
$$

따라서 사원수 $q = e^{\frac{\theta}{2}\hat{n}} = cos\frac{\theta}{2} + \hat{n}sin\frac{\theta}{2}$ 라면 식 $e^{\frac{\theta}{2}\hat{n}} v e^{-\frac{\theta}{2}\hat{n}}$ 을 $qvq^{\*}$ 라 쓸 수 있고 이는 축 $\hat{n}$ 에 대해 $\theta$ 만큼 회전한 $v'$ 입니다.

$$
v' = qvq^{\*}
$$

<br><br>
### 쿼터니언 회전 행렬

단위 사원수 $q$ 와 벡터 $v$ 에 대한 식 $qvq^{\*}$ 을 이용해 쿼터니언 회전 행렬을 구해보겠습니다. 먼저 $qvq^{\*}$ 을 직접 전개합니다. (아래 전개식은 단순화 되었습니다. 전체 전개 과정이 상당히 복잡하니 {% link 참고링크 http://www.weizmann.ac.il/sci-tea/benari/sites/sci-tea.benari/files/uploads/softwareAndLearningMaterials/quaternion-tutorial-2-0-1.pdf %}의 부록을 보시는 것을 추천드립니다.)

$$
\begin{align}
qvq^{\*} & = (q_0 + q_1i + q_2j + q_3k)(v_1i + v_2j + v_3k)(q_0 - q_1i - q_2j - q_3k) \\\
& = (v_1(q_0^2 + q_1^2 - q_2^2 - q_3^2) + 2v_2(q_1 q_2 - q_0 q_3) + 2v_3(q_0 q_2 + q_1 q_3))i \\\
& + (2v_1(q_0 q_3 + q_1 q_2) + v_2(q_0^2 - q_1^2 + q_2^2 - q_3^2) + 2v_3(q_2 q_3 - q_0 q_1))j \\\
& + (2v_1(q_1 q_3 - q_0 q_2) + 2v_2(q_0 q_1 + q_2 q_3) + v_3(q_0^2 - q_1^2 - q_2^2 + q_3^2))k
\end{align}
$$

전개식을 행렬로 변환하면 다음과 같습니다.

$$
Mv = \begin{bmatrix}
q_0^2 + q_1^2 - q_2^2 - q_3^2 & 2(q_1 q_2 - q_0 q_3) & 2(q_0 q_2 + q_1 q_3) \\\
2(q_0 q_3 + q_1 q_2) & q_0^2 - q_1^2 + q_2^2 - q_3^2 & 2(q_2 q_3 - q_0 q_1) \\\
2(q_1 q_3 - q_0 q_2) & 2(q_0 q_1 + q_2 q_3) & q_0^2 - q_1^2 - q_2^2 + q_3^2
\end{bmatrix}
\begin{bmatrix}
v_1 \\\
v_2 \\\
v_3
\end{bmatrix}
$$

$q$ 는 단위 사원수이므로 $q_0^2 + q_1^2 + q_2^2 + q_3^2 = 1$ 입니다. 이를 이용해 행렬 $M$ 을 변형할 수 있습니다.

$$
M = 2 \cdot \begin{bmatrix}
q_0^2 + q_1^2 - 0.5 & q_1 q_2 - q_0 q_3 & q_0 q_2 + q_1 q_3 \\\
q_0 q_3 + q_1 q_2 & q_0^2 + q_2^2 - 0.5 & q_2 q_3 - q_0 q_1 \\\
q_1 q_3 - q_0 q_2 & q_0 q_1 + q_2 q_3 & q_0^2 + q_3^2 - 0.5
\end{bmatrix}
$$


이번에는 회전 행렬 $M$ 을 이용해 사원수의 원소 $q_0, q_1, q_2, q_3$ 를 구하는 식을 유도해 보겠습니다. 행렬의 대각원소의 합 $Trace(M)$ 을 이용해 다음과 같이 쓸 수 있습니다.

$$
\begin{align}
Trace(M) & = 2(3q_0^2 + q_1^2 + q_2^2 + q_3^2 - 1.5) \\\
& = 2(3q_0^2 + (1 - q_0^2) - 1.5) \\\
& = 4q_0^2 - 1 \\\ \\\
& \therefore |q_0| = \sqrt{\frac{Trace(M) + 1}{4}}
\end{align}
$$

$q_1$ 은 $M_{11} = 2(q_0^2 + q_1^2 - 0.5)$ 임을 이용해 구할 수 있습니다.

$$
\begin{align}
M_{11} & = 2(q_0^2 + q_1^2 - 0.5) \\\ \\\
& = 2\left(\frac{Trace(M) + 1}{4} + q_1^2 - 0.5\right) \\\ \\\
\end{align}
$$

$$
\frac{M_{11}}{2} = \frac{Trace(M) + 1}{4} + q_1^2 - \frac{1}{2} \\\ \\\
q_1^2 = \frac{M_{11}}{2} + \frac{1 - Trace(M)}{4} \\\ \\\
\therefore |q_1| = \sqrt{\frac{M_{11}}{2} + \frac{1 - Trace(M)}{4}}
$$

같은 방법으로 $M_{22}$, $M_{33}$ 을 이용해 $q_2$, $q_3$ 를 구할 수 있습니다.

<br><br>
한편 쿼터니언 회전 행렬을 오일러 회전 행렬과 같게 두어 오일러 회전의 $x$, $y$, $z$ 축에 대한 회전에 대응하는 사원수의 원소를 구할 수 있습니다. 아래는 $z$ 축에 대해 $\psi$ 만큼 회전하게 하는 행렬입니다.

$$
M = \begin{bmatrix}
cos\psi & -sin\psi & 0 \\\
sin\psi & cos\psi & 0 \\\
0 & 0 & 1
\end{bmatrix}
$$

사원수의 원소 $|q_0|$ 에 대한 식 $|q_0| = \sqrt{\frac{Trace(M) + 1}{4}}$ 에 행렬의 $Trace(M) = cos\psi + cos\psi + 1$ 을 대입하면 $|q_0|$ 을 찾을 수 있습니다.

$$
\begin{align}
|q_0| & = \sqrt{\frac{2cos\psi + 2}{4}} \\\ \\\
& = \sqrt{\frac{cos\psi + 1}{2}} \\\ \\\
& = \sqrt{\frac{cos^2\frac{\psi}{2} - sin^2\frac{\psi}{2} + 1}{2}} \\\ \\\
& = \sqrt{cos^2\frac{\psi}{2}} \\\ \\\
& = cos\frac{\psi}{2}
\end{align}
$$

아래 식은 $q_1$, $q_2$, $q_3$ 를 구하는 과정입니다.

$$
\begin{align}
|q_1| & = \sqrt{\frac{M_{11}}{2} + \frac{1 - Trace(M)}{4}} \\\ \\\
& = \sqrt{\frac{cos\psi}{2} + \frac{1 - (2cos\psi + 1)}{4}} \\\ \\\
& = 0
\end{align}
$$

$$
\begin{align}
|q_2| & = \sqrt{\frac{M_{22}}{2} + \frac{1 - Trace(M)}{4}} \\\ \\\
& = \sqrt{\frac{cos\psi}{2} + \frac{1 - (2cos\psi + 1)}{4}} \\\ \\\
& = 0
\end{align}
$$

$$
\begin{align}
|q_3| & = \sqrt{\frac{M_{33}}{2} + \frac{1 - Trace(M)}{4}} \\\ \\\
& = \sqrt{\frac{1}{2} + \frac{1 - (2cos\psi + 1)}{4}} \\\ \\\
& = \sqrt{\frac{1 - cos\psi}{2}} \\\ \\\
& = \sqrt{\frac{1 - (cos^2\frac{\psi}{2} - sin^2\frac{\psi}{2})}{2}} \\\ \\\
& = \sqrt{sin^2\frac{\psi}{2}} \\\ \\\
& = sin\frac{\psi}{2}
\end{align}
$$

따라서 행렬 $M = \begin{bmatrix}
cos\psi & -sin\psi & 0 \\\
sin\psi & cos\psi & 0 \\\
0 & 0 & 1
\end{bmatrix}$ 에 대응하는 사원수는 $q = cos\frac{\psi}{2} + ksin\frac{\psi}{2}$ 입니다.

위의 변환과정을 활용해 오일러 회전에서의 roll ($x$ 축 회전), pitch ($y$ 축 회전), yaw ($z$ 축 회전) 을 사원수의 곱으로 표현해 보겠습니다.

roll 회전각을 $\phi$, pitch 회전각을 $\theta$, yaw 회전각을 $\psi$ 라 하면 아래와 같은 사원수의 곱을 얻습니다.

$$
\left(cos\frac{\psi}{2} + k\text{ }sin\frac{\psi}{2}\right)
\left(cos\frac{\theta}{2} + j\text{ }sin\frac{\theta}{2}\right)
\left(cos\frac{\phi}{2} + i\text{ }sin\frac{\phi}{2}\right) \\\
= cos\frac{\phi}{2} cos\frac{\theta}{2} cos\frac{\psi}{2} + sin\frac{\phi}{2} sin\frac{\theta}{2} sin\frac{\psi}{2} + \\\
\left( sin\frac{\phi}{2} cos\frac{\theta}{2} cos\frac{\psi}{2} - cos\frac{\phi}{2} sin\frac{\theta}{2} sin\frac{\psi}{2} \right)i + \\\
\left( cos\frac{\phi}{2} sin\frac{\theta}{2} cos\frac{\psi}{2} + sin\frac{\phi}{2} cos\frac{\theta}{2} sin\frac{\psi}{2} \right)j + \\\
\left( cos\frac{\phi}{2} cos\frac{\theta}{2} sin\frac{\psi}{2} + sin\frac{\phi}{2} sin\frac{\theta}{2} cos\frac{\psi}{2} \right)k
$$

전개식은 어렵지 않게 유도할 수 있습니다.

roll, pitch, yaw 를 알고 있을 때 위 식을 통해 $q$ 를 찾고 $q$ 에 허수부의 부호를 달리한 $q^{-1}$ 을 찾습니다. 마지막으로 세 사원수의 곱 $qvq^{-1}$ 으로 벡터 $v$ 에 대한 회전을 구합니다.

<br><br><br>
참고 : http://www.weizmann.ac.il/sci-tea/benari/sites/sci-tea.benari/files/uploads/softwareAndLearningMaterials/quaternion-tutorial-2-0-1.pdf
참고 : https://www.youtube.com/watch?v=q-ESzg03mQc
참고 : https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles