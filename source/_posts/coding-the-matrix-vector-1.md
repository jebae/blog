---
title: 코딩 더 매트릭스 - 3장 벡터 (1)
date: 2018-11-18 09:40:37
categories:
- 수학
- 코딩 더 매트릭스
tags:
- 수학
- 코딩 더 매트릭스
- python
---

필립 클라인의 저서 *코딩 더 매트릭스* 3장 벡터.

---
1. 벡터의 성질을 이용해 2차원 좌표평면에 선분을 그려 봅니다.

2. 볼록결합과 아핀결합에 대해 알아봅니다.

3. 선형방정식과 선형시스템을 알아봅니다.
---

<br>
### 선분 그리기

2차원 좌표위에 꼬리가 원점이고 머리가 $(a, b)$인 벡터 $v$를 그리려 합니다. 선은 점들의 집합이니 원점과 좌표 $(a, b)$를 지나는 직선에서 $v$의 꼬리에서 머리 사이 점들을 찍으면 벡터 $v$를 그릴 수 있습니다. 이 점들의 집합은 $\\{\alpha v : \alpha \in \Bbb{R}, 0 \le \alpha \le 1 \\}$라 쓸 수 있습니다. 여기서 $\alpha$는 벡터 $v$를 스케일링하는 스칼라입니다. 스칼라 $\alpha$가 0과 1사이에서 정의되니 범위안에서 100개의 샘플을 뽑아 그려보겠습니다.

```python
from plotting import plot

def scalar_vector_mult(alpha, v):
    return [ alpha * v[i] for i in range(len(v)) ]

v = [2, 1]
plot([scalar_vector_mult(a / 100, v) for a in range(101)])
```
<center>
<img src="https://www.dropbox.com/s/hxmsy0m9pqgxl5m/vector_line.png?raw=1"
width="50%"/>
</center>

만약 $\alpha$의 범위가 실수 전체라면 원점과 $(a, b)$를 지나는 직선을 그릴 수 있을 것입니다. 
<br>
위에서 그렸던 선분은 벡터의 덧셈을 이용해 평행이동 할 수 있습니다.

```python
def add2(v1, v2):
	return [ a + b for a, b in zip(v1, v2) ]

plot([add2(scalar_vector_mult(a / 100, v), [0.5, 1])  for a in range(101)])
```

<center>
<img src="https://www.dropbox.com/s/qck83mgv0ze40og/vector_line_move.png?raw=1"
width="50%"/>
<span class="image-caption">$(0.5, 1)$만큼 평행이동</span>
</center>

이 선분은 원점에서 $(2, 1)$까지의 선분을 $(0.5, 1)$만큼 평행이동한 것이기도 하지만 $(0.5, 1)$에서 $(2.5, 2)$까지의 선분이기도 합니다. 즉 두 점 $u$, $v$를 끝점으로 가지는 선분은 다음과 같이 쓸 수 도 있습니다.
$$\\{ \alpha (v - u) + u : \alpha \in \Bbb{R}, 0 \le \alpha \le 1 \\}$$

<br><br>
### 볼록결합과 아핀결합
$\\{ \alpha (v - u) + u : \alpha \in \Bbb{R}, 0 \le \alpha \le 1 \\}$ 식을 이용해 두 점을 양 끝점으로 가지는 선분을 나타낼 수 있었습니다. 이 식을 조금 더 변형해보겠습니다. 
$$\begin{align} \alpha (v - u) + u & = \alpha v - \alpha u + u \\\ & = \alpha v + (1 - \alpha)u \end{align}$$
위 식에서 $1 - \alpha = \beta $ 로 치환해 보겠습니다.
$$ \\{ \alpha v + \beta u : \alpha, \beta \in \Bbb{R}, \alpha, \beta \ge 0, \alpha + \beta = 1 \\} $$
이와 같은 표현식을 벡터 $u$와 $v$의 **볼록결합 (Convex combination)**이라 합니다.
볼록결합 표현식을 이용해 다시 한번 선분을 그려보겠습니다.

```python
def segment(pt1, pt2):
    result = []
    for a in range(11):
		# alpha x v, a를 10으로 나눠주면서 0 <= alpha <= 1 조건을 충족함
        a_pt1 = scalar_vector_mult(a / 10, pt1)
		# beta x u
        a_pt2 = scalar_vector_mult(1 - a / 10, pt2)
        result.append(add2(a_pt1, a_pt2))
    plot(result)

segment([0.5, 1], [2.5, 2])
```
<center>
<img src="https://www.dropbox.com/s/o63z0qvdofc28c1/vector_convex.png?raw=1"
width="50%"/>
</center>

두 점을 지나는 직선의 점들의 집합은 선분의 점들의 집합에서 $0 \le \alpha \le 1$ 조건만 빼면 나타낼 수 있습니다. 임의의 벡터 $u$에 대해 모든 실수 스칼라곱을 할 수 있다면 당연 $u$의 방향 또는 그 정반대 방향으로의 스케일링된 벡터들을 구할 수 있습니다. 볼록결합의 표현식에서는 $\alpha, \beta \ge 0$ 조건을 빼서 같은 의미를 나타냅니다.
$$ \\{ \alpha v + \beta u : \alpha, \beta \in \Bbb{R}, \alpha + \beta = 1 \\} $$
이와 같은 표현식을 벡터 $u$와 $v$의 **아핀결합 (Affine combination)**이라 합니다.

<center>
<img src="https://www.dropbox.com/s/d2pwhsdb5mjsdp9/vector_affine.png?raw=1"
width="50%"/>
</center>

<br><br>
### 선형방정식과 선형시스템

선형방정식과 선형시스템은 선형대수학을 공부할때 가장 많이 접하는 용어입니다. 선형방정식은 $a \cdot x = \beta$ 의 형태를 가지는 식으로 $a$는 벡터, $\beta$는 스칼라이며 $x$는 구하고자 하는 선형방정식의 해 벡터입니다. 선형시스템은 그런 방정식들을 모아놓은 컬렉션입니다.
$$
a_1 \cdot x = \beta _1
\\\ a_2 \cdot x = \beta _2
\\\ a_3 \cdot x = \beta _3
\\\ .
\\\ .
\\\ .
\\\ a_n \cdot x = \beta _n
$$
벡터 $x$는 위와 같은 선형시스템의 선형방정식들을 모두 만족해야합니다. 선형시스템의 해가 존재하는지, 존재한다면 유일한지를 판단하는 것은 선형대수학으로 문제를 해결할때 주요 관심사이기도 합니다.