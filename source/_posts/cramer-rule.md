---
title: 크라머 공식 (Cramer's rule)
tags:
  - 수학
  - 선형대수학
categories:
  - 수학
  - 선형대수학
date: 2019-08-04 10:12:48
---


### 크라머 공식 (Cramer's rule)

$$
\begin{bmatrix}
a_{11} & a_{12} & ... & a_{1n} \\\
a_{21} & a_{22} & ... & a_{2n} \\\
& & ... & \\\
a_{n1} & a_{n2} & ... & a_{nn} \\\
\end{bmatrix}
\begin{bmatrix}
x_1 \\\
x_2 \\\
... \\\
x_n
\end{bmatrix}
= \begin{bmatrix}
b_1 \\\
b_2 \\\
... \\\
b_n
\end{bmatrix}
$$

위와 같은 행렬방정식 $Ax = b$ 가 있습니다. 행렬 $A$ 의 한 열 $j$ 를 열벡터 $b$ 로 대체한 행렬을 $A_j$ 라 하겠습니다. 이 때 해벡터 $x$ 의 한 해 $x_j$에 대해 다음이 성립합니다. 

$$
x_j = \frac{det\text{ }A_j}{det\text{ }A}
$$

행렬식 $Ax = b$ 에서 해벡터 $x$ 의 전체 엘리먼트가 아닌 필요한 해 $x_j$ 만을 구하는 위 식을 **크라머 공식**이라 합니다.

<br><br>
### 증명

행렬 $A$ 의 $ij$ 여인자를 $C_{ij}$ 라 할 때 행렬식 $det\text{ }A$ 를 다음과 같이 쓸 수 있습니다.

$$
\begin{align}
det\text{ }A & = a_{1j}C_{1j} + a_{2j}C_{2j} + ... + a_{nj}C_{nj} \\\
& = \sum^n_{i = 1}a_{ij}C_{ij}
\end{align}
$$

한편 $k \neq j$ 인 $k$ 에 대해 다음과 같이 식을 써보겠습니다.

$$
a_{1k}C_{1j} + a_{2k}C_{2j} + ... + a_{nk}C_{nj} = \sum^n_{i = 1}a_{ik}C_{ij}
$$

위 식에서 여인자 $C_{ij}$ 에 $k$ 열의 값이 반영되어 있으므로 이는 $k$ 열과 값이 같은 열이 적어도 하나 이상 더 존재하는 행렬의 행렬식과 같습니다. 값이 같은 열이 두 개이상 존재하면 행렬의 열벡터들이 일차독립이 아니므로 위 식의 결과는 0입니다.

$$
\sum^n_{i = 1}a_{ik}C_{ij} = 0
$$

<br>
이제 아래와 같이 행렬방정식을 전개해 보겠습니다.

$$
a_{11}x_1 + a_{12}x_2 + ... + a_{1j}x_j + ... + a_{1n}x_n = b_1 \\\
a_{21}x_1 + a_{22}x_2 + ... + a_{2j}x_j + ... + a_{2n}x_n = b_2 \\\
... \\\
a_{n1}x_1 + a_{n2}x_2 + ... + a_{nj}x_j + ... + a_{nn}x_n = b_n
$$

각 식에 여인자 $C_{ij}$ 를 곱해보겠습니다.

$$
a_{11}C_{1j}x_1 + a_{12}C_{1j}x_2 + ... + a_{1j}C_{1j}x_j + ... + a_{1n}C_{1j}x_n = b_1 C_{1j} \\\
a_{21}C_{2j}x_1 + a_{22}C_{2j}x_2 + ... + a_{2j}C_{2j}x_j + ... + a_{2n}C_{2j}x_n = b_2 C_{2j} \\\
... \\\
a_{n1}C_{nj}x_1 + a_{n2}C_{nj}x_2 + ... + a_{nj}C_{nj}x_j + ... + a_{nn}C_{nj}x_n = b_n C_{nj}
$$

위 식들을 모두 더해보겠습니다.

$$
\sum^n_{i = 1}a_{i1}C_{ij}x_1 + \sum^n_{i = 1}a_{i2}C_{ij}x_2 + ... + \sum^n_{i = 1}a_{ij}C_{ij}x_j + ... + \sum^n_{i = 1}a_{in}C_{ij}x_n
= \sum^n_{i = 1}b_i C_{ij}
$$

식에서 $k \neq j$ 인 $k$ 열에 대해 $\sum^n_{i = 1}a_{ik}C_{ij} = 0$ 이므로 $\sum^n_{i = 1}a_{ik}C_{ij}x$ 와 같은 항은 모두 0입니다. $\sum^n_{i = 1}a_{ij}C_{ij}x_j = det\text{ }A x_j$ 이고 $\sum^n_{i = 1}b_i C_{ij}$ 는 행렬 $A$ 의 $j$ 열을 벡터 $b$ 로 대채했을 때의 행렬식 $det\text{ }A_j$ 입니다. 따라서 식을 정리하면 다음과 같습니다.

$$
0 + 0 + ... + det\text{ }A x_j + ... + 0 = det\text{ }A_j \\\
det\text{ }A x_j = det\text{ }A_j \\\
\therefore x_j = \frac{det\text{ }A_j}{det\text{ }A}
$$

<br><br><br>
출처 : https://freshrimpsushi.tistory.com/783