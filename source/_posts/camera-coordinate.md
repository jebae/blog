---
title: 카메라 좌표계 변환
tags:
  - 그래픽스
  - 벡터
categories:
  - 알고리즘
  - 그래픽스
date: 2019-06-16 17:09:34
---


### 좌표계 변환

좌표계의 축, 즉 기저를 $x$, $y$, $z$ 라 할 때 한 지점 $p$ 의 좌표 표현 $(p_x, p_y, p_z)$ 은 아래와 같이 쓸 수 있습니다.

$$
\begin{bmatrix}
\text{ } & \text{ } & \text{ } \\\
x & y & z\\\
\text{ } & \text{ } & \text{ }
\end{bmatrix}
\begin{bmatrix}
p_x \\\
p_y \\\
p_z
\end{bmatrix}
= p
$$

공간상의 지점 $p$ 는 그 위치는 변하지 않고 좌표축에 따라 좌표 표현이 달라집니다. 새로운 좌표계의 축을 $u$, $v$, $w$ 이라 할 때 아래와 같이 쓸 수 있습니다.

$$
\begin{bmatrix}
\text{ } & \text{ } & \text{ } \\\
u & v & w \\\
\text{ } & \text{ } & \text{ }
\end{bmatrix}
\begin{bmatrix}
p_u \\\
p_v \\\
p_w
\end{bmatrix}
=
\begin{bmatrix}
\text{ } & \text{ } & \text{ } \\\
x & y & z\\\
\text{ } & \text{ } & \text{ }
\end{bmatrix}
\begin{bmatrix}
p_x \\\
p_y \\\
p_z
\end{bmatrix}
= p
$$

만약 좌표축 $x$, $y$, $z$ 에 대한 좌표 표현 $p_x$, $p_y$, $p_z$ 로 부터 좌표축 $u$, $v$, $w$ 에 대한 좌표 표현 $p_u$, $p_v$, $p_w$ 를 얻고자 한다면 행렬 $\begin{bmatrix}
\text{ } & \text{ } & \text{ } \\\
u & v & w \\\
\text{ } & \text{ } & \text{ }
\end{bmatrix}$ 의 역행렬을 이용합니다.

$$
\begin{bmatrix}
p_u \\\
p_v \\\
p_w
\end{bmatrix}
= \begin{bmatrix}
\text{ } & \text{ } & \text{ } \\\
u & v & w \\\
\text{ } & \text{ } & \text{ }
\end{bmatrix}^{-1}
\begin{bmatrix}
\text{ } & \text{ } & \text{ } \\\
x & y & z\\\
\text{ } & \text{ } & \text{ }
\end{bmatrix}
\begin{bmatrix}
p_x \\\
p_y \\\
p_z
\end{bmatrix}
$$

이 때 행렬 $U = \begin{bmatrix}
\text{ } & \text{ } & \text{ } \\\
u & v & w \\\
\text{ } & \text{ } & \text{ }
\end{bmatrix}$ 의 열벡터들은 좌표축, 즉 직교기저이고 각 열벡터를 정규화(normalized) 한다면 $U^{-1} = U^T$ 이 성립합니다.

<br><br>
### 카메라 좌표계 유도

물체의 위치를 표현할 때 기준으로 삼는 좌표계를 **월드 좌표계**라 합니다. 가장 흔하게는 $x = (1, 0, 0)$, $y = (0, 1, 0)$, $z = (0, 0, 1)$ 을 축으로 가지고 행렬로 $\begin{bmatrix}
1 & 0 & 0 \\\
0 & 1 & 0 \\\
0 & 0 & 1
\end{bmatrix}$ 로 표현할 수 있는 좌표계가 있습니다.

**카메라 좌표계**는 카메라의 초점을 기준으로 하는 좌표계입니다. 카메라의 초점이 원점이 되고 이에 대해 물체의 좌표 표현이 정해집니다. 카메라 좌표계의 $z$ 축의 방향은 사람의 눈이 모니터를 바라보는 방향과 같습니다. 아래 그림은 월드 좌표계의 축 $x_w$, $y_w$, $z_w$ 과 카메라 좌표계의 축 $x_c$, $y_c$, $z_c$, 물체의 위치 $P$ 를 보여주고 있습니다. 카메라 좌표계의 축 $x_c$, $y_c$, $z_c$ 은 월드 좌표계 상 표현되는 단위 벡터입니다.

<center>
<img src="https://www.dropbox.com/s/4f3tgi5vyn9y5wa/camera_coord1.jpg?raw=1" width="70%"/>
</center>

월드 좌표계상 물체의 위치 $P_w$ 가 카메라를 기준으로 어떤 위치에 놓이는지를 생각하면 좌표계 변환의 이해가 쉽습니다. 월드 좌표계의 원점을 $O_w$ 라 하고 월드 좌표계상 카메라의 위치를 $C_w$ 라 할 때 아래 그림은 물체의 위치를 $-\overline{O_w C_w}$ 만큼 이동시킨 결과 입니다.

<center>
<img src="https://www.dropbox.com/s/5809trxeq24u8pv/camera_coord_translate.jpg?raw=1" width="80%"/>
</center>

이후 월드 좌표계의 축을 아래와 같이 카메라 좌표계의 각 축의 방향과 일치하도록 회전시키면 물체의 좌표는 카메라 좌표계상 위치 $P_c$ 가 됩니다.

<center>
<img src="https://www.dropbox.com/s/s1bb43l328f7xro/camera_coord_rotate.jpg?raw=1" width="80%"/>
</center>

카메라 좌표계 변환은 간단하게 한번의 평행이동과 회전으로 이루어 집니다. 이제 위의 과정을 하나의 행렬식으로 표현해 보겠습니다. 카메라 좌표축을 행렬 $C$ 라 하고 월드 좌표계를 행렬 $W$ 라 하면 다음이 항상 성립합니다.

$$
CP_c = WP_w
$$

간단하게 아래와 같이 카메라 좌표축의 역행렬을 양변에 곱해 물체의 카메라 좌표상 위치를 구할 수 있다고 **착각**할 수 있습니다.

$$
P_c = C^{-1}WP_w
$$

우리가 간과한 것은 행렬 $C$ 와 $W$ 가 열벡터로 가지고 있는 좌표축이 벡터일 뿐이고 벡터의 시작점, 즉 원점에 대한 정보는 가지고 있지 않다는 것입니다. 원점을 고려하지 않았으므로 위 식은 카메라의 위치가 월드 좌표계의 원점일 때만 성립합니다. 카메라 좌표계 변환을 적용하기 위해서는 아래와 같이 월드 좌표계에 대한 물체의 위치를 $-\overline{O_wC_w}$ 만큼 평행이동시켜 줍니다.

$$
CP_c = WP_w - \begin{bmatrix}
c_x \\\
c_y \\\
c_z
\end{bmatrix}
$$

물체의 위치를 $-\overline{O_wC_w}$ 만큼 평행이동 시킨 것은 카메라 위치를 월드 좌표계의 원점으로 이동시킨 것과 같습니다. 따라서 평행이동한 물체의 위치에 대해 $C^{-1}$ 을 곱하면 물체의 카메라 좌표상 위치를 알 수 있습니다.

$$
P_c = C^{-1}\left(WP_w - \begin{bmatrix}
c_x \\\
c_y \\\
c_z
\end{bmatrix}
\right)
$$

$C^{-1} = C^{T}$ 이므로 아래와 같이 다시 쓸 수 있습니다.

$$
P_c = C^{T}\left(WP_w - \begin{bmatrix}
c_x \\\
c_y \\\
c_z
\end{bmatrix}
\right)
$$

평행이동을 고려해 4벡터 행렬로 표현하면 아래와 같습니다.

$$
P_c = \begin{bmatrix}
\text{ } & x_c & \text{ } & 0 \\\
\text{ } & y_c & \text{ } & 0 \\\
\text{ } & z_c & \text{ } & 0 \\\
0 & 0 & 0 & 1
\end{bmatrix}
\begin{bmatrix}
1 & 0 & 0 & -c_x \\\
0 & 1 & 0 & -c_y \\\
0 & 0 & 1 & -c_z \\\
0 & 0 & 0 & 1
\end{bmatrix}
P_w
$$

월드 좌표계 축들의 행렬 $W$ 는 단위행렬이므로 위 식에서는 생략되었습니다.

<br><br>
### 카메라 좌표계 축

<center>
<img src="https://www.dropbox.com/s/u3equ1rqngd6frm/camera_space.jpg?raw=1" width="80%"/>
</center>

월드 좌표계 상 카메라의 위치는 주어지는 정보입니다. 하지만 카메라 좌표축은 카메라의 위치와 카메라가 바라보는 지점 (focus)를 고려해 직접 찾아야 합니다. focus 는 게임 캐릭터가 바라보는 방향(1인칭 시점), 공중에서 캐릭터를 바라보는 방향 등 (전지적 시점) 정하기 나름입니다. 이 글에서는 편의상 물체 $P$ 의 위치를 focus 라 하겠습니다. $z_c$ 축은 카메라가 focus 를 바라보는 방향, 즉 $\overline{CP}$ 입니다.

카메라가 아래 그림과 같이 좌우로 기울어지지 않고 focus 를 바라본다고 하겠습니다.

<center>
<img src="https://www.dropbox.com/s/zh6lrghouthp551/camera_focus.jpg?raw=1" width="50%"/>
</center>

이 때 카메라 좌표축 중 $x_c$ 축은 카메라 좌표축 $z_c$ 와 월드 좌표축 $z_w$ 의 외적 $z_c \times z_w$ 입니다. 벡터 $z_c$ 의 시작점을 월드 좌표계 상 원점에 가져다 놓고 오른손 법칙을 쓰면 쉽게 이해할 수 있습니다. 만약 카메라가 좌우로 기울어졌다면 벡터 $x_c$ 를 $z_c$ 를 축으로 원하는 각도만큼 회전시키면 됩니다. 이는 {% post_link rodrigues-rotation 로드리게스 회전 %} 또는 {% post_link quaternion-rotation 쿼터니언 회전 %} 으로 구현할 수 있습니다.

$y_c$ 축은 $z_c \times x_c$ 입니다. 모든 축을 정규화 시켜 아래 식에 대입하면 카메라 좌표계 변환을 위한 식이 완성됩니다.

$$
P_c = \begin{bmatrix}
\text{ } & x_c & \text{ } & 0 \\\
\text{ } & y_c & \text{ } & 0 \\\
\text{ } & z_c & \text{ } & 0 \\\
0 & 0 & 0 & 1
\end{bmatrix}
\begin{bmatrix}
1 & 0 & 0 & -c_x \\\
0 & 1 & 0 & -c_y \\\
0 & 0 & 1 & -c_z \\\
0 & 0 & 0 & 1
\end{bmatrix}
P_w
$$

<br><br><br>
참고 : http://www.cse.psu.edu/~rtc12/CSE486/lecture12.pdf